"done case
syn region Structure start=/+/ end=/\n/

"description on case
syn region Keyword start=/'/ end=/'/

"high priority case
syn region Error start=/!/ end=/\n/

"comments
syn region Comment start=/#/ end=/\n/
