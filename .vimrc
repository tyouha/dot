colorscheme molokai
set nocompatible
	
" source $VIMRUNTIME/vimrc_example.vim

"4 tabs as space
filetype plugin indent on

set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set smartindent

"mouse enable
"set mouse=a

set hlsearch
set incsearch

"set number
set hlsearch
set showmatch
set history=1000
set cursorline


set t_Co=256
set ts=2
set sw=2
"Asdfasdf

filetype on
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()


set ignorecase
syntax on

set noswapfile
set backupdir=~/.vim/backup
set nobackup
set nowritebackup


set fileencodings=utf8,cp1251

set wildmenu
set wildmode=list:longest
set wcm=<Tab>

set viminfo='20,<50,s10,h,rA:,rB:


"set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P
set guitablabel=%-200.400f

" avoid hit enter stupud messages
set cmdheight=1

" show status always
set laststatus=2

" zfz
set rtp+=/usr/local/opt/fzf

" wrap
set nowrap
set textwidth=0
set paste


" set colors for todo and note files
au BufRead,BufNewFile *.todo set filetype=todo
au BufRead,BufNewFile *.note set filetype=note
